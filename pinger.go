// Written by Sean Cox - 4 AUG 2018
// Simple connectivity program written in GO to demonstrate networking and concurrency abilities

package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// Global variables
var hostsList []Host                                              // Create the pointer to the hosts list in a global variable
var templates = template.Must(template.ParseFiles("status.html")) // Create template for status HTML page

// Define a structure to hold information about a host
type Host struct {
	Hostname string
	Port     int
	IsUp     bool
}

// checkConn is a "receiver method" for the Host structure.
// It checks connection to the host and sets the isUp member appropriately
func (h *Host) checkConn() {
	_, err := net.Dial("tcp", fmt.Sprintf("%s:%d", h.Hostname, h.Port))

	if err != nil { // An error has occured
		h.IsUp = false
	}
	h.IsUp = true
}

// loadHosts takes an input file and returns an array of hosts that are in the file
// inFile string - the name of the file in which the hosts are listed
// It is expected that the input file is a list of hosts:ip (eg. www.google.com:80) with each host:ip pair on a seperate line
func loadHosts(inFile string) []Host {
	fileBody, err := ioutil.ReadFile(inFile)

	if err != nil { // Couldn't read file so panic
		panic(err)
	}

	lines := strings.Split(string(fileBody), "\n") // Split the file text based on the new line character to get each line

	hList := make([]Host, len(lines)) // Create an array whose length is the number of lines, this array represents a list of hosts

	for i := 0; i < len(lines); i++ {
		line := string(lines[i])
		splitLine := strings.Split(line, ":") // Split the file line on the ':' character
		hostName := splitLine[0]
		port, err := strconv.Atoi(splitLine[1]) // Convert to integer using ATOI function, capture error just in case

		if err != nil {
			log.Printf("Could not convert port number to int from %s. Defaulting to port 80. \n", line)
			port = 80 // Default to port 80
		}
		h := Host{Hostname: hostName, Port: port, IsUp: false}
		hList[i] = h // Add the host to the hosts array
	}

	return hList

}

// runWebServer starts the web server and processes HTTP requests.
// The web server portion of this application is used to inform end users the status of the hosts
func runWebServer() {
	http.HandleFunc("/status", handleStatusRequest) // Set the function to handle calls to /status endpoint
	log.Fatal(http.ListenAndServe(":8080", nil))
}

// handleStatusRequest handles requests for the /status endpoint and returns an HTML template with host statuses
func handleStatusRequest(w http.ResponseWriter, r *http.Request) {
	log.Println(hostsList)
	templates.Execute(w, hostsList) // Return the template with hostsList data.
}

func main() {
	const scanTime = 60        // How often to perform checks in seconds
	const inFile = "hosts.txt" // The text file where host names will be read in from
	log.Println("Application started.")
	hostsList = loadHosts(inFile)
	log.Printf("Hosts file loaded. Hosts: %v \n", hostsList)
	go runWebServer()
	// Scan the hosts
	doExit := false
	for doExit == false {
		log.Println("Checking hosts.")
		for i := 0; i < len(hostsList); i++ {
			hostsList[i].checkConn()
		}
		time.Sleep(scanTime * time.Second) // Sleep until next scan
	}

	log.Printf("Hosts status: %v \n", hostsList)

}
